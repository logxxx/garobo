module garabo.logxxx

go 1.14

require (
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.7
	github.com/go-vgo/robotgo v0.100.10
	github.com/google/uuid v1.3.0
	github.com/gorilla/websocket v1.5.0
	github.com/kbinani/screenshot v0.0.0-20210720154843-7d3a670d8329
	github.com/nfnt/resize v0.0.0-20180221191011-83c6a9932646
	github.com/nsqio/go-nsq v1.1.0
	github.com/stretchr/testify v1.7.0 // indirect
	golang.org/x/image v0.0.0-20211028202545-6944b10bf410
)
