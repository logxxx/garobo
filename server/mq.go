package server

import (
	"bytes"
	"encoding/json"
	"fmt"
	"garabo.logxxx/pkg/handlemsg"
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/pkg/net"
	"garabo.logxxx/proto"
	"io/ioutil"
	"net/http"
)

func StartMqProxy() error {

	mqHost := "150.158.99.30"
	err := handlemsg.InitMessageHandler(mqHost)
	if err != nil {
		return err
	}

	h := handlemsg.GetMessageHandler()

	topic := fmt.Sprintf("%v_%v_%v", net.GetHostName(), "server", "request")
	log.Infof("MQ Listening topic %v...", topic)
	err = h.StartConsume(topic, "server", consumeFunc)

	if err != nil {
		return err
	}

	return nil

}

func consumeFunc(data []byte) error {

	msg := &proto.MqMessage{}
	err := json.Unmarshal(data, msg)
	if err != nil {
		return err
	}
	log.Infof("server recv msg respTo:%v router:%v data:%v",
		msg.ResponseTo, msg.Router, string(msg.Data))

	//根据router，转发到对应api
	url := fmt.Sprintf("http://localhost:%v%v", GetConfig().ApiPort, msg.Router)
	log.Infof("router url:%v", url)

	bodyBuf := bytes.NewBuffer(msg.Data)
	httpReq, err := http.NewRequest("GET", url, bodyBuf)
	if err != nil {
		return err
	}

	resp, err := http.DefaultClient.Do(httpReq)
	if err != nil {
		return err
	}
	defer resp.Body.Close()

	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	log.Infof("server request local[%v] succ. len(resp):%v", url, len(respBytes))

	//回信
	back := &proto.MqMessage{
		Router: msg.Router,
		Data:   respBytes,
	}

	err = handlemsg.GetMessageHandler().JSON(msg.ResponseTo, back)
	if err != nil {
		return err
	}
	log.Infof("server send back to topic[%v] succ", msg.ResponseTo)

	return nil
}
