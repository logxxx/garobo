package ioctrl

import (
	"errors"
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/pkg/reqresp"
	"garabo.logxxx/proto"
	"garabo.logxxx/server/io/iobiz"
	"github.com/gin-gonic/gin"
	"strconv"
)

func ControlMouseToggle(c *gin.Context) {
	req := &proto.MouseToggleReq{}

	reqresp.ParseReq(c, req)

	iobiz.MouseToggle(req.IsRight, req.IsUp)

	resp := &proto.ControlResp{
		Message: "ok",
	}

	reqresp.MakeResp(c, resp)

}

func ControlMouseClick(c *gin.Context) {

	req := &proto.ControlReq{}

	reqresp.ParseReq(c, req)

	if req.MouseClick != "" && req.MouseClick != "left" && req.MouseClick != "right" {
		reqresp.MakeClientErr(c, errors.New("invalid mouse click"))
		return
	}

	isRight := req.MouseClick == "right"

	iobiz.ClickMouse(isRight)

	resp := &proto.ControlResp{
		Message: "ok",
	}

	reqresp.MakeResp(c, resp)

}

func ControlSetMousePos(c *gin.Context) {
	log.Infof("into ControlSetMousePos")

	req := &proto.ControlReq{}

	reqresp.ParseReq(c, req)

	if req.SetMousePosX == 0 && req.SetMousePosY == 0 {
		setXStr := c.Query("x")
		setYStr := c.Query("y")
		req.SetMousePosX, _ = strconv.Atoi(setXStr)
		req.SetMousePosY, _ = strconv.Atoi(setYStr)
	}

	if req.SetMousePosX == 0 || req.SetMousePosY == 0 {
		reqresp.MakeClientErr(c, errors.New("empty x or y"))
		return
	}

	log.Infof("ControlSetMousePos x:%v y:%v", req.SetMousePosX, req.SetMousePosY)

	iobiz.SetMousePos(req.SetMousePosX, req.SetMousePosY)

	resp := &proto.ControlResp{
		Message: "ok",
	}

	reqresp.MakeResp(c, resp)

}

func ControlMouseMove(c *gin.Context) {

	log.Infof("into ControlMouseMove")

	req := &proto.ControlReq{}

	reqresp.ParseReq(c, req)

	if req.MouseMoveX == 0 && req.MouseMoveY == 0 {
		moveXStr := c.Query("move_x")
		moveYStr := c.Query("move_y")
		req.MouseMoveX, _ = strconv.Atoi(moveXStr)
		req.MouseMoveY, _ = strconv.Atoi(moveYStr)
	}

	if req.MouseMoveX == 0 && req.MouseMoveY == 0 {
		reqresp.MakeClientErr(c, errors.New("empty x and y"))
		return
	}

	log.Infof("ControlMouse x:%v y:%v", req.MouseMoveX, req.MouseMoveY)

	iobiz.MoveMouse(req.MouseMoveX, req.MouseMoveY)

	resp := &proto.ControlResp{
		Message: "ok",
	}

	reqresp.MakeResp(c, resp)
}
