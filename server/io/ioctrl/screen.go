package ioctrl

import (
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/pkg/reqresp"
	"garabo.logxxx/proto"
	"garabo.logxxx/server/io/iobiz"
	"github.com/gin-gonic/gin"
)

//获取屏幕分辨率
func GetScreenResolution(c *gin.Context) {
	width, height := iobiz.GetScreenResolution()

	resp := &proto.GetScreenResolutionResp{
		Width:  width,
		Height: height,
	}

	reqresp.MakeResp(c, resp)

}

// 连续截屏
func CaptureContinuously(c *gin.Context) {
	log.Infof("recv capture_continuously request!")

	req := &proto.CaptureContinuouslyReq{}

	reqresp.ParseReq(c, req)

	if req.Stop {
		iobiz.GetCaptureContinuouslyHandler().Close()
	} else {
		iobiz.GetCaptureContinuouslyHandler().Start()
	}

}
