package ioctrl

import (
	"errors"
	"garabo.logxxx/pkg/reqresp"
	"garabo.logxxx/proto"
	"garabo.logxxx/server/io/iobiz"
	"github.com/gin-gonic/gin"
)

func ControlKeyboardInput(c *gin.Context) {

	req := &proto.ControlReq{}

	reqresp.ParseReq(c, req)

	if req.InputWord == "" {
		req.InputWord = c.Query("input_words")
	}

	if req.InputWord == "" {
		reqresp.MakeClientErr(c, errors.New("empty input word"))
		return
	}

	if len(req.InputWord) > 1 {
		reqresp.MakeClientErr(c, errors.New("just allow one word"))
		return
	}

	err := iobiz.InputWord(req.InputWord)
	if err != nil {
		reqresp.MakeServerErr(c, err)
		return
	}

	resp := &proto.ControlResp{
		Message: "ok",
	}

	reqresp.MakeResp(c, resp)

}
