package iobiz

import (
	"bytes"
	"garabo.logxxx/pkg/log"
	"github.com/go-vgo/robotgo"
	"github.com/kbinani/screenshot"
	"github.com/nfnt/resize"
	"image"
	"image/draw"
	"image/jpeg"
	"image/png"
	"os"
)

func DrawScreenOnly() ([]byte, error) {

	//1.截屏
	shotImg, err := screenshot.CaptureDisplay(0)
	if err != nil {
		log.Errorf("GetScreenshotWithMouse CaptureDisplay err:%v", err)
		return nil, err
	}

	buf := bytes.NewBufferString("")

	err = jpeg.Encode(buf, shotImg, &jpeg.Options{Quality: 30})
	if err != nil {
		log.Errorf("GetScreenshotWithMouse Encode err:%v", err)
		return nil, err
	}

	return buf.Bytes(), nil

}

func DrawScreenWithMouse() ([]byte, error) {

	//1.截屏
	shotImg, err := screenshot.CaptureDisplay(0)
	if err != nil {
		log.Errorf("GetScreenshotWithMouse CaptureDisplay err:%v", err)
		return nil, err
	}

	cursorFile, err := os.Open("./server/asset/cursor_20x20.png")
	if err != nil {
		log.Errorf("GetScreenshotWithMouse Open err:%v", err)
		return nil, err
	}
	defer cursorFile.Close()

	cursorImg, err := png.Decode(cursorFile)
	if err != nil {
		log.Errorf("GetScreenshotWithMouse Decode err:%v", err)
		return nil, err
	}

	mouseX, mouseY := robotgo.GetMousePos()

	rect := image.Rect(mouseX, mouseY, mouseX+50, mouseY+50)
	draw.Draw(shotImg, rect, cursorImg, image.Point{}, draw.Over)

	resizedImg := resize.Resize(1280, 720, shotImg, resize.Lanczos3)

	buf := bytes.NewBufferString("")

	err = jpeg.Encode(buf, resizedImg, &jpeg.Options{Quality: 30})
	if err != nil {
		log.Errorf("GetScreenshotWithMouse Encode err:%v", err)
		return nil, err
	}

	return buf.Bytes(), nil

}
