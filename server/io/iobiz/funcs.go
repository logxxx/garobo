package iobiz

import (
	"encoding/base64"
	"garabo.logxxx/pkg/log"
	"github.com/go-vgo/robotgo"
	"github.com/kbinani/screenshot"
	"golang.org/x/image/colornames"
	"image"
	"image/draw"
	"syscall"
)

const (
	SM_CXSREEN  = uintptr(0) //x
	SM_CYSCREEN = uintptr(1) //y
)

func GetScreenResolution() (int, int) {
	w, _, _ := syscall.NewLazyDLL("user32.dll").NewProc("GetSystemMetrics").Call(SM_CXSREEN)
	h, _, _ := syscall.NewLazyDLL("user32.dll").NewProc("GetSystemMetrics").Call(SM_CYSCREEN)
	return int(w), int(h)
}

func GetCapture() (*image.RGBA, error) {
	//1.GetCapture
	shotImg, err := screenshot.CaptureDisplay(0)
	if err != nil {
		log.Errorf("GetScreenshotWithMouse CaptureDisplay err:%v", err)
		return nil, err
	}
	return shotImg, nil
}

func ConvertCaptureToBytes(shotImg *image.RGBA) ([]byte, error) {
	//2.get mouse img
	mouseX, mouseY := robotgo.GetMousePos()
	mouseRect := image.Rect(mouseX-10, mouseY-10, mouseX+20, mouseY+20)

	//3.合成
	draw.Draw(shotImg, mouseRect, &image.Uniform{colornames.Red}, image.ZP, draw.Src)

	data, err := encodeImg(shotImg)
	if err != nil {
		log.Errorf("GetScreenshotWithMouse encodeImg err:%v", err)
		return nil, err
	}

	return data, nil
}

func ConvertCaptureToBase64(shotImg *image.RGBA) ([]byte, error) {

	data, err := ConvertCaptureToBytes(shotImg)
	if err != nil {
		return nil, err
	}

	dataBase64 := make([]byte, base64.StdEncoding.EncodedLen(len(data)))
	base64.StdEncoding.Encode(dataBase64, data)

	return dataBase64, nil
}

func GetScreenshotWithMouse() ([]byte, error) {

	img, err := GetCapture()
	if err != nil {
		return nil, err
	}

	return ConvertCaptureToBytes(img)
}
