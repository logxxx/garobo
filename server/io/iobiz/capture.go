package iobiz

import (
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/server/utils/proxy"
	"time"
)

var (
	_captureContinuouslyHandler = &CaptureContinuouslyHandler{}
)

type CaptureContinuouslyHandler struct {
	IsRunning bool
}

func GetCaptureContinuouslyHandler() *CaptureContinuouslyHandler {
	return _captureContinuouslyHandler
}

func (h *CaptureContinuouslyHandler) Start() {

	if h.IsRunning { //幂等
		return
	}

	h.IsRunning = true

	go func() {
		runCount := int64(0)
		st := time.Now()
		for {
			if !h.IsRunning {
				return
			}
			runCount++
			if runCount%10 == 0 {
				runSec := int64(time.Now().Sub(st).Seconds())
				if runSec > 0 {
					log.Infof("已连续截图%vs，共%v帧，fps=%v", runSec, runCount, runCount/runSec)
				}
				if runSec > 600 { //10分钟保底
					return
				}
			}

			captureData, err := DrawScreenOnly()
			if err != nil {
				log.Errorf("CaptureContinuouslyHandler.Start DrawScreenWithMouse err:%v", err)
				continue
			}

			//func() {

			//	err = handlemsg.GetMessageHandler().Bytes("capture_continuously", imgData)
			//	if err != nil {
			//		log.Infof("capture_continuously Bytes err:%v", err)
			//	}
			//}()

			err = proxy.SendToProxy(captureData)
			if err != nil {
				log.Errorf("CaptureContinuouslyHandler.Start SendToProxy err:%v", err)
				return
			}

			//log.Infof("CaptureContinuouslyHandler.Start SendToProxy succ!")

		}
	}()

}

func (h *CaptureContinuouslyHandler) Close() {
	h.IsRunning = false
}
