package iobiz

import (
	"bytes"
	"image"
	"image/jpeg"
)

func encodeImg(source *image.RGBA) ([]byte, error) {
	buf := bytes.NewBufferString("")
	err := jpeg.Encode(buf, source, &jpeg.Options{Quality: 30})
	if err != nil {
		return nil, err
	}
	return buf.Bytes(), nil
}
