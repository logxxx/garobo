package iobiz

import (
	"github.com/go-vgo/robotgo"
	"log"
)

func SetMousePos(x, y int) {
	robotgo.Move(x, y)
}

func MoveMouse(x, y int) {
	mouseX, mouseY := robotgo.GetMousePos()
	robotgo.MoveSmooth(mouseX+x, mouseY+y)
}

func ClickMouse(isRight bool) {
	if isRight {
		robotgo.Click("right")
	} else {
		robotgo.Click()
	}

}

func MouseToggle(isRight bool, isUp bool) {

	args := make([]string, 0)

	arg1 := ""
	if isRight {
		arg1 = "right"
	}
	args = append(args, arg1)

	arg2 := ""
	if isUp {
		arg2 = "up"
	}
	if arg2 != "" {
		args = append(args, arg2)
	}
	log.Printf("MouseToggle args:%v", args)
	robotgo.Toggle(args...)
}

func InputWord(words string) error {

	for _, word := range words {
		_ = robotgo.KeyTap(string(word))
	}

	return nil
}
