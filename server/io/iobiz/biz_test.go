package iobiz

import (
	"garabo.logxxx/pkg/log"
	"io/ioutil"
	"testing"
	"time"
)

func TestMouseToggle(t *testing.T) {
	time.Sleep(3 * time.Second)
	log.Infof("start toggle!")
	MouseToggle(false, false)
	time.Sleep(3 * time.Second)
	log.Infof("toggle up!")
	MouseToggle(false, true)
}

func TestInputWord(t *testing.T) {
	time.Sleep(3 * time.Second)
	log.Infof("start input word!")
	for i := 0; i < 5; i++ {
		time.Sleep(100 * time.Millisecond)
		InputWord("hello")
	}
	log.Infof("input finish!")
}

func TestGetScreenResolution(t *testing.T) {
	w, h := GetScreenResolution()
	t.Logf("w:%v h:%v", w, h)
}

func TestSetMousePos(t *testing.T) {

	var x int = 0
	var y int = 0

	time.Sleep(1 * time.Second)
	x = 20
	y = 20
	t.Logf("to %v %v", x, y)
	SetMousePos(x, y)

	time.Sleep(2 * time.Second)
	x = 220
	y = 20
	t.Logf("to %v %v", x, y)
	SetMousePos(x, y)

	time.Sleep(2 * time.Second)
	x = 220
	y = 620
	t.Logf("to %v %v", x, y)
	SetMousePos(x, y)

	time.Sleep(2 * time.Second)
	x = 420
	y = 620
	t.Logf("to %v %v", x, y)
	SetMousePos(x, y)

	time.Sleep(2 * time.Second)
	x = 960
	y = 540
	t.Logf("to %v %v", x, y)
	SetMousePos(x, y)
}

func TestDrawScreenWithMouse(t *testing.T) {
	_, err := DrawScreenWithMouse()
	if err != nil {
		t.Fatal(err)
	}
}

func TestDrawScreenOnly(t *testing.T) {
	resp, err := DrawScreenOnly()
	if err != nil {
		t.Fatal(err)
	}
	ioutil.WriteFile("./resp1.jpg", resp, 0777)
}
