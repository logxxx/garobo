package proxy

import (
	"errors"
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/simpleproxy/proxyclient"
	"sync"
)

var (
	lock         sync.Mutex
	_proxyClient *proxyclient.ProxyClient
)

func initProxy() error {
	lock.Lock()
	defer lock.Unlock()
	proxyAddr := "150.158.99.30:7331"
	var err error
	_proxyClient, err = proxyclient.NewClient(proxyAddr, nil)
	if err != nil {
		log.Errorf("initProxy NewClient err:%v", err)
		return err
	}
	return nil
}

func SendToProxy(data []byte) error {
	if _proxyClient == nil {
		err := initProxy()
		if err != nil {
			return err
		}
	}

	lock.Lock()
	defer lock.Unlock()

	if _proxyClient == nil {
		return errors.New("_proxyClient still nil")
	}

	err := _proxyClient.Send(data)
	if err != nil {
		log.Errorf("SendToProxy Send err:%v", err)
		return err
	}

	return nil
}
