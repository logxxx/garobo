package server

var (
	_cfg *GlobalConfig
)

type GlobalConfig struct {
	ApiPort string
}

func InitConfig() {
	_cfg = &GlobalConfig{}
}

func GetConfig() *GlobalConfig {
	return _cfg
}