package server

import (
	"garabo.logxxx/pkg/reqresp"
	"garabo.logxxx/server/io/iobiz"
	"garabo.logxxx/server/io/ioctrl"
	"github.com/gin-gonic/gin"
	"time"
)

func RegisterApi() {
	g := gin.Default()

	g.Use(reqresp.Print)

	g.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
		return
	})

	g.GET("/server_time", func(c *gin.Context) {
		c.String(200, time.Now().Format("2006-01-02 15:04:05"))
		return
	})

	g.GET("/capture_continuously", ioctrl.CaptureContinuously)

	g.GET("/capture", func(c *gin.Context) {
		data, err := iobiz.DrawScreenWithMouse()
		if err != nil {
			c.JSON(500, err.Error())
			return
		}
		c.Writer.Write(data)
		return
	})

	g.GET("/screen_resolution", ioctrl.GetScreenResolution)

	g.GET("/control/mouse/move", ioctrl.ControlMouseMove)

	g.GET("/control/mouse/set_pos", ioctrl.ControlSetMousePos)

	g.GET("/control/mouse/click", ioctrl.ControlMouseClick)

	g.GET("/control/mouse/toggle", ioctrl.ControlMouseToggle)

	g.GET("/control/keyboard/input", ioctrl.ControlKeyboardInput)

	g.Run(":" + GetConfig().ApiPort)

}
