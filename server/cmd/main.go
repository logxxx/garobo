package main

import (
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/pkg/runutil"
	"garabo.logxxx/server"
	"os"
)

// 目标:提供一个页面，能展示当前屏幕
// 提供五个按钮:上移，下移，左移，右移，点击
// 提供一个输入框和一个按钮，按钮为:写入

func main() {

	wd, _ := os.Getwd()
	log.Infof("main wd:%v", wd)

	server.InitConfig()

	server.GetConfig().ApiPort = "8081"

	go server.RegisterApi()

	err := server.StartMqProxy()
	if err != nil {
		panic(err)
	}

	runutil.WaitForExit()
}
