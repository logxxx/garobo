package persistent

import (
	"context"
	"fmt"
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/pkg/runutil"
	"net"
	"runtime"
	"strings"
)

//长链接库

func NewTcpServer(addr string, handleFunc func(ctx context.Context, conn net.Conn, msg []byte) error) error {

	ctx := context.Background()

	tcpAddr, err := net.ResolveTCPAddr("tcp", addr)
	if err != nil {
		log.Errorf("NewTcpServer ResolveTCPAddr err:%v p1:%v", err, addr)
		return err
	}

	listener, err := net.ListenTCP("tcp", tcpAddr)
	if err != nil {
		log.Errorf("NewTcpServer net.ListenTCP err:%v p1:%+v", err, tcpAddr)
		return err
	}

	for {
		log.Infof("here NewTcpServer for start")
		conn, err := listener.AcceptTCP() //这里会阻塞，直到有新的连接到来
		if err != nil {
			log.Errorf("NewTcpServer Accept() err:%v", err)
			if nerr, ok := err.(net.Error); ok && nerr.Temporary() {
				log.Infof("is Temporary err, so continue.")
				runtime.Gosched()
				continue
			}
			// theres no direct way to detect this error because it is not exposed
			if !strings.Contains(err.Error(), "use of closed network connection") {
				err = fmt.Errorf("listener.Accept() error - %v", err)
				log.Errorf("NewTcpServer Accept() err:%v", err)
				return err
			}
			break
		}
		log.Infof("NewTcpServer Get A New Conn!localAddr:%v remoteAddr:%v", conn.LocalAddr().String(), conn.RemoteAddr().String())

		runutil.RunSafe(func() {
			HandleConn(ctx, conn, handleFunc)
		})
	}
	return nil
}
