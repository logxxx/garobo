package main

import (
	"context"
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/pkg/persistent"
	"net"
	"strings"
	"time"
)

var (
	clientAddress = ":7777"
	serverAddress = ":7788"
)

func main() {

	ctx := context.Background()

	log.Infof("start server...")
	//server
	go func() {

		err := persistent.NewTcpServer(serverAddress, func(ctx context.Context, conn net.Conn, msg []byte) error {
			log.Infof("server recv msg:%v", string(msg))
			return nil
		})
		if err != nil {
			log.Infof("server persistent.NewTcpServer err:%v", err)
			return
		}

	}()

	time.Sleep(1 * time.Second)

	log.Infof("start client...")
	//client
	go func() {

		clientAddr, _ := net.ResolveTCPAddr("tcp", clientAddress)
		serverAddr, _ := net.ResolveTCPAddr("tcp", serverAddress)

		connToMaster, err := net.DialTCP("tcp", clientAddr, serverAddr)
		if err != nil {
			log.Errorf("TestHandleConn net.DialTCP err:%v p1:%v p2:%v",
				err.Error(), clientAddress, serverAddress)
			return
		}
		defer connToMaster.Close()

		if connToMaster != nil {
			log.Infof("client conn to server succ.")
		}

		go func() {

			t := time.Tick(1 * time.Second)
			for {
				select {
				case <-t:
					msg := strings.Repeat("1234567890", 3000)
					err := persistent.Send(ctx, connToMaster, msg)
					if err != nil {
						log.Errorf("client persistent.Send err:%v", err)
					}
					log.Infof("client send msg len:%v msg:%v", len(msg), msg)
					t = time.Tick(30 * time.Second)
				}
			}
		}()

		persistent.HandleConn(ctx, connToMaster, func(ctx context.Context, conn net.Conn, msg []byte) error {
			log.Infof("client recv msg:%v", string(msg))
			return nil
		})

		log.Infof("client HandleConn finish.")

	}()

	select {}

}
