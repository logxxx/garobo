package persistent_test

import (
	"bufio"
	"bytes"
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/pkg/persistent"

	"testing"
)

func TestPacketSplitFunc(t *testing.T) {
	content1 := `{"kind":"detect","direction":"00","req":{"node":{"id":"eagleeye-service-canary-7955996455-lbnjf","name":"eagleeye-service-canary-7955996455-lbnjf","inner_ip":"172.21.40.21","extern_ip":"47.100.108.242","in_cluster":false,"create_time":0,"update_time":0},"request_id":"841199d2-c893-4222-a8b4-dd9007e54673","content":"{\"task_id\":66,\"task_name\":\"搜索服务-（IOS）搜索接口（通过uid搜索用户）\",\"details\":{\"UidSearchForIos\":{\"detail_id\":119,\"detail_name\":\"UidSearchForIos\",\"task_id\":66,\"task_name\":\"搜索服务-（IOS）搜索接口（通过uid搜索用户）\",\"project_id\":\"2rvk4e3gkdnl7u1kl0k\",\"client_id\":\"Xp6vsxz_7IYVw2BB\",\"sort_id\":1,\"template\":\"{\\n  \\\"checktype\\\": \\\"random\\\",\\n  \\\"urls\\\": [\\n    \\\"https://api-shoulei-ssl.xunlei.com/xlppc.searcher.api/v1/ios/search?keyword=209829499\\\"\\n  ],\\n  \\\"request\\\": {\\n    \\\"headers\\\": {\\n      \\\"accept\\\": \\\"*/*\\\",\\n      \\\"x-client-version-code\\\": \\\"13680\\\",\\n      \\\"x-client-id\\\": \\\"Xp6vsxz_7IYVw2BB\\\",\\n      \\\"x-device-id\\\": \\\"96e4a03a12aa3ff6a183b703c1577f6f\\\",\\n      \\\"user-agent\\\": \\\"ANDROID-com.xunlei.downloadprovider/7.31.0.8199.994 netWorkType/4G appid/40 deviceName/Samsung_Sm-g9500 deviceModel/SM-G9500 OSVersion/9 protocolVersion/301 platformVersion/10 sdkVersion/219500 Oauth2Client/0.9 (Linux 4_4_153-16246896) (JAVA 0)\\\",\\n      \\\"x-channel-id\\\": \\\"0x10800001\\\",\\n      \\\"x-peer-id\\\": \\\"1ce06208e74ce6c4682b5f88f41ccb46\\\",\\n      \\\"x-guid\\\": \\\"1ce06208e74ce6c4682b5f88f41ccb46\\\",\\n      \\\"User-Id\\\": \\\"5123000061\\\"\\n    },\\n    \\\"resolv\\\": {},\\n    \\\"body\\\": {},\\n    \\\"method\\\": \\\"get\\\"\\n  },\\n  \\\"judge\\\": [\\n    \\\"http.status==200\\\",\\n    \\\"code==0\\\",\\n    \\\"result=='ok'\\\",\\n    \\\"user_info.uid\\u003e0\\\",\\n    \\\"user_info.nick_name\\u003e0\\\",\\n    \\\"user_info.newno\\u003e0\\\"\\n  ]\\n}\",\"remark\":\"\",\"create_time\":0,\"update_time\":0,\"is_del\":0}},\"step_order\":[\"UidSearchForIos\"]}"},"resp":null}`
	content2 := `{"kind":"detect","direction":"00","req":{"node":{"id":"eagleeye-service-canary-7955996455-lbnjf","name":"eagleeye-service-canary-7955996455-lbnjf","inner_ip":"172.21.40.21","extern_ip":"47.100.108.242","in_cluster":false,"create_time":0,"update_time":0},"request_id":"6f4cc2e1-9f6b-4736-8c4c-c6e39555260c","content":"{\"task_id\":65,\"task_name\":\"搜索服务-（IOS）请求短视频、公众号等接口（分页拉取的）\",\"details\":{\"ResSearchForIos\":{\"detail_id\":118,\"detail_name\":\"ResSearchForIos\",\"task_id\":65,\"task_name\":\"搜索服务-（IOS）请求短视频、公众号等接口（分页拉取的）\",\"project_id\":\"2rvk4e3gkdnl7u1kl0k\",\"client_id\":\"Xp6vsxz_7IYVw2BB\",\"sort_id\":1,\"template\":\"{\\n\\t\\\"checktype\\\": \\\"random\\\",\\n\\t\\\"urls\\\": [\\n\\t\\t\\\"https://api-shoulei-ssl.xunlei.com/xlppc.searcher.api/v1/ios/res_search?pagelen=10\\u0026page=1\\u0026type=album\\u0026keyword=%E6%88%91\\\"\\n\\t],\\n\\t\\\"request\\\": {\\n\\t\\t\\\"headers\\\": {\\n\\t\\t\\t\\\"accept\\\": \\\"*/*\\\",\\n\\t\\t\\t\\\"x-client-version-code\\\": \\\"13680\\\",\\n\\t\\t\\t\\\"x-client-id\\\": \\\"Xp6vsxz_7IYVw2BB\\\",\\n\\t\\t\\t\\\"x-device-id\\\": \\\"96e4a03a12aa3ff6a183b703c1577f6f\\\",\\n\\t\\t\\t\\\"user-agent\\\": \\\"ANDROID-com.xunlei.downloadprovider/7.31.0.8199.994 netWorkType/4G appid/40 deviceName/Samsung_Sm-g9500 deviceModel/SM-G9500 OSVersion/9 protocolVersion/301 platformVersion/10 sdkVersion/219500 Oauth2Client/0.9 (Linux 4_4_153-16246896) (JAVA 0)\\\",\\n\\t\\t\\t\\\"x-channel-id\\\": \\\"0x10800001\\\",\\n\\t\\t\\t\\\"x-peer-id\\\": \\\"1ce06208e74ce6c4682b5f88f41ccb46\\\",\\n\\t\\t\\t\\\"x-guid\\\": \\\"1ce06208e74ce6c4682b5f88f41ccb46\\\",\\n\\t\\t\\t\\\"User-Id\\\": \\\"5123000061\\\"\\n\\t\\t},\\n\\t\\t\\\"resolv\\\": {},\\n\\t\\t\\\"body\\\": {},\\n\\t\\t\\\"method\\\": \\\"get\\\"\\n\\t},\\n\\t\\\"judge\\\": [\\n\\t\\t\\\"http.status==200\\\",\\n\\t\\t\\\"result==0\\\"\\n\\t]\\n}\",\"remark\":\"\",\"create_time\":0,\"update_time\":0,\"is_del\":0}},\"step_order\":[\"ResSearchForIos\"]}"},"resp":null}`

	packet1 := persistent.GenePacket([]byte(content1))
	packet2 := persistent.GenePacket([]byte(content2))

	data := append(packet1, packet2...)

	scanner := bufio.NewScanner(bytes.NewBuffer(data))
	scanner.Split(persistent.PacketSplitFunc)
	for scanner.Scan() {
		fullPkg := scanner.Bytes()[8:]
		log.Infof("recv msg:%v", string(fullPkg))
	}
}
