package persistent

import (
	"bytes"
	"context"
	"encoding/binary"
	"encoding/json"
	"errors"
	"garabo.logxxx/pkg/log"
	"net"
)

func GenePacket(rawData []byte) []byte {
	//组装数据
	magicNum := make([]byte, 4)
	binary.BigEndian.PutUint32(magicNum, 0x123456)
	lenNum := make([]byte, 4)
	lenData := uint32(len(rawData))
	binary.BigEndian.PutUint32(lenNum, lenData)

	packetBuf := bytes.NewBuffer(magicNum)
	packetBuf.Write(lenNum)
	packetBuf.Write(rawData)

	return packetBuf.Bytes()
}

func CreateConn(remoteAddr string) (net.Conn, error) {
	conn, err := net.Dial("tcp", remoteAddr)
	if err != nil {
		log.Errorf("CreateConn Dial err:%v remoteAddr",
			err.Error(), remoteAddr)
		return nil, err
	}
	return conn, nil
}

func Send(ctx context.Context, client net.Conn, obj interface{}) error {

	//编码数据
	data, err := json.Marshal(obj)
	if err != nil {
		return err
	}

	packet := GenePacket(data)

	//发送数据
	nlen, err := client.Write(packet)
	if err != nil {
		log.WithCtx(ctx).Errorf("Send client.Write err:%v", err)
		return err
	}
	if nlen != len(packet) {
		err = errors.New("send less")
		log.WithCtx(ctx).Errorf("Send client.Write err:%v", err)
		return err
	}

	//log.WithCtx(ctx).Infof("Send Succ:%v len:%v", string(packet), len(packet))

	return nil
}
