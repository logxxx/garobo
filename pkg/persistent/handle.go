package persistent

import (
	"bytes"
	"context"
	"encoding/binary"
	"errors"
	"fmt"
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/pkg/runutil"

	"io"
	"net"
	"time"
)

var (
	packageMaxLen = 65536 //尽量设置大一点，否则容易被其他请求打断被当成脏请求。尤其是保活探测容易冲突
)

func HandleConn(ctx context.Context, conn net.Conn, handleFunc func(ctx context.Context, conn net.Conn, msg []byte) error) {
	ctx, cancel := context.WithCancel(ctx)
	defer cancel()
	for {
		//先读header
		headData := make([]byte, 8)
		_, err := io.ReadFull(conn, headData)
		if err != nil {
			log.WithCtx(ctx).Infof("HandleConn ReadFull headData err:%v", err)
			break
		}
		//解析header
		msgLen, err := getMsgLen(headData)
		if err != nil {
			log.WithCtx(ctx).Infof("HandleConn getMsgLen err:%v", err)
			break
		}
		//log.Infof("HandleConn get msgLen:%v", msgLen)

		if msgLen <= 0 {
			time.Sleep(1 * time.Second)
			continue
		}

		//有数据
		msg := make([]byte, msgLen)

		_, err = io.ReadFull(conn, msg)
		if err != nil {
			log.WithCtx(ctx).Infof("HandleConn ReadFull msg err:%v", err)
			break
		}

		runutil.RunSafe(func() {
			if handleFunc == nil {
				log.WithCtx(ctx).Errorf("HandleConn handleFunc err:%v", "handleFunc == nil")
				return
			}
			err := handleFunc(ctx, conn, msg)
			if err != nil {
				log.WithCtx(ctx).Errorf("HandleConn handleFunc err:%v msg:%v", err, string(msg))
			}

		})

	}

}

func getMsgLen(header []byte) (uint32, error) {
	if len(header) < 8 {
		return 0, errors.New("invalid header len")
	}

	headerBuf := bytes.NewReader(header)

	//读magic number
	var magicNumber uint32
	if err := binary.Read(headerBuf, binary.BigEndian, &magicNumber); err != nil {
		return 0, errors.New("parse magicNumber failed")
	}
	if magicNumber != 0x123456 {
		return 0, fmt.Errorf("invalid magicNumber:%v except:%v", magicNumber, 0x123456)
	}

	//读消息长度
	var msgLen uint32
	if err := binary.Read(headerBuf, binary.BigEndian, &msgLen); err != nil {
		return 0, errors.New("parse msgLen failed")
	}

	return msgLen, nil

}

func PacketSplitFunc(data []byte, atEOF bool) (advance int, token []byte, err error) {
	if atEOF {
		return
	}
	if len(data) < 8 {
		return
	}
	if binary.BigEndian.Uint32(data[:4]) != 0x123456 {
		return
	}
	log.Infof("PacketSplitFunc msg len:%v", binary.BigEndian.Uint32(data[4:8]))
	var msgLen int32
	err = binary.Read(bytes.NewReader(data[4:8]), binary.BigEndian, &msgLen)
	if err != nil {
		log.Errorf("PacketSplitFunc binary.Read err:%v data:%v", err, string(data))
		return
	}
	packageLen := int(msgLen) + 8
	if packageLen <= len(data) {
		if packageLen <= 0 {
			err = fmt.Errorf("PacketSplitFunc failed: packageLen=%v", packageLen)
			log.Errorf("PacketSplitFunc err:%v", err)
			return
		}
		log.Infof("data[:8]:%v", string(data[:8]))
		return packageLen, data[:packageLen], nil
	}
	return
}
