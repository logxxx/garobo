package handlemsg

// 消息队列
// 负责接收消息数据。
//

import (
	"encoding/json"
	"errors"
	"fmt"
	"garabo.logxxx/pkg/log"
	"github.com/nsqio/go-nsq"
)

var (
	_msgHandler *MessageHandler
)

type MessageHandler struct {
	host      string
	producer  *nsq.Producer
	consumers map[string]*nsq.Consumer
}

func InitMessageHandler(host string) error {
	_msgHandler = NewMessageHandler(host)

	err := _msgHandler.Init()
	if err != nil {
		return err
	}

	return nil
}

func GetMessageHandler() *MessageHandler {
	return _msgHandler
}

func NewMessageHandler(host string) *MessageHandler {
	return &MessageHandler{
		host:      host,
		consumers: make(map[string]*nsq.Consumer),
	}
}

func (h *MessageHandler) JSON(topic string, data interface{}) error {
	dataBytes, err := json.Marshal(data)
	if err != nil {
		return err
	}

	err = h.producer.Publish(topic, dataBytes)
	if err != nil {
		return err
	}

	return nil
}

func (h *MessageHandler) Bytes(topic string, data []byte) error {
	err := h.producer.Publish(topic, data)
	if err != nil {
		return err
	}

	return nil
}

func (h *MessageHandler) String(topic string, data string) error {

	err := h.producer.Publish(topic, []byte(data))
	if err != nil {
		return err
	}

	return nil
}

func (h *MessageHandler) StartConsume(topic string, channel string, f func(data []byte) error) error {

	key := fmt.Sprintf("%v_%v", topic, channel)

	_, ok := h.consumers[key]
	if ok {
		return errors.New("consumer already exist")
	}

	//2.创建消费者
	cfg := nsq.NewConfig()
	consumer, err := nsq.NewConsumer(topic, channel, cfg)
	if err != nil {
		return err
	}

	log.Infof("[mq]start to consumer topic:%v", topic)

	h.consumers[key] = consumer

	//注册处理函数
	consumer.AddHandler(nsq.HandlerFunc(func(msg *nsq.Message) error {
		log.Infof("[mq] recv msg: len=%v", len(msg.Body))
		err := f(msg.Body)
		msg.Finish()
		return err
	}))

	//通过lookupd 发现生产者
	err = consumer.ConnectToNSQLookupd(h.host + ":4161")
	if err != nil {
		return err
	}

	return nil
}

func (h *MessageHandler) Init() error {

	//1.创建生产者
	cfg := nsq.NewConfig()
	producer, err := nsq.NewProducer(h.host+":4150", cfg)
	if err != nil {
		return err
	}

	err = producer.Ping()
	if err != nil {
		return err
	}

	h.producer = producer

	return nil

}

func (h *MessageHandler) Send() {

}

func (h *MessageHandler) Recv() {

}
