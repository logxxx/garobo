package imageutil

import "encoding/base64"

func ToBase64(data []byte) []byte {
	if len(data) <= 0 {
		return nil
	}
	dataBase64 := make([]byte, base64.StdEncoding.EncodedLen(len(data)))
	base64.StdEncoding.Encode(dataBase64, data)
	return dataBase64
}
