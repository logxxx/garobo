package log

import "log"

func Info(v ...interface{}) {
	log.Print(v...)
}

func Infof(format string, v ...interface{}) {
	log.Printf(format, v...)
}

func Debugf(format string, v ...interface{}) {
	log.Printf(format, v...)
}

func Error(v ...interface{}) {
	log.Print(v...)
}

func Errorf(format string, v ...interface{}) {
	log.Printf(format, v...)
}
