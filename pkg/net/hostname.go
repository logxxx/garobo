package net

import "os"

var (
	hostName string
)

func init() {
	hostName, _ = os.Hostname()
}

func GetHostName() string {
	//return "DESKTOP-JBEDR7B"
	return hostName
}
