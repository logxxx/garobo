package runutil

import (
	"fmt"
	"garabo.logxxx/pkg/log"
	"os"
	"os/signal"
	"runtime"
	"syscall"
)

func WaitForExit(closeFunc ...func()) {
	doneChan := make(chan bool)
	signalChan := make(chan os.Signal, 1)
	signal.Notify(signalChan, syscall.SIGINT, syscall.SIGTERM)
	for {
		select {
		case s := <-signalChan:
			fmt.Printf("captured %v. exiting...\n", s)
			if closeFunc != nil {
				for _, f := range closeFunc {
					f()
				}
			}
			close(doneChan)
		case <-doneChan:
			os.Exit(0)
		}
	}
}

// 安全地执行goroutine。包装了recover()捕获异常
func RunSafe(fn func()) {
	if fn == nil {
		return
	}
	go func() {
		defer func() {
			if err := recover(); err != nil {
				buf := make([]byte, 1024)
				_ = runtime.Stack(buf, false)
				log.Errorf("RunSafe panic:%v stack:%s", err, buf)
			}
		}()

		fn()
	}()
}
