package reqresp

import (
	"bytes"
	"fmt"
	"garabo.logxxx/pkg/log"
	"github.com/gin-gonic/gin"
	"io/ioutil"
	"time"
)

func Print(c *gin.Context) {
	st := time.Now()

	rawReq, _ := c.GetRawData()

	c.Request.Body = ioutil.NopCloser(bytes.NewReader(rawReq))

	c.Next()

	showResp := ""
	respObj, ok := c.Get(CtxFieldResp)
	if ok {
		showResp = fmt.Sprintf("%v", respObj)
		if len(showResp) > ShowRespMaxLen {
			showResp = showResp[:ShowRespMaxLen] + fmt.Sprintf("...(%v more words ignored)", len(showResp)-ShowRespMaxLen)
		}
	}

	d := time.Since(st)

	log.Infof("[Req]url:%v st=%.2fs rawReq:%v rawResp:%v",
		c.Request.URL.String(), d.Seconds(), string(rawReq), showResp)
}
