package reqresp

import (
	"encoding/json"
	"garabo.logxxx/pkg/log"
	"github.com/gin-gonic/gin"
)

const (
	ShowRespMaxLen = 512
	CtxFieldResp   = "rawRespJson"
)

func ParseReq(c *gin.Context, req interface{}) error {
	rawData, err := c.GetRawData()
	if err != nil {
		return err
	}
	log.Debugf("ParseReq rawData:%v", string(rawData))

	err = json.Unmarshal(rawData, req)
	if err != nil {
		return err
	}

	return nil
}

func makeResp(c *gin.Context, code int, obj interface{}) {
	respJson, err := json.Marshal(obj)
	if err != nil {
		log.Errorf("makeResp err:%v obj:%v", err, obj)
	}
	c.Set(CtxFieldResp, string(respJson))
	c.JSON(code, obj)
}

func MakeResp(c *gin.Context, obj interface{}) {
	makeResp(c, 200, obj)
}

func MakeServerErr(c *gin.Context, obj error) {
	makeResp(c, 500, obj)
}

func MakeClientErr(c *gin.Context, obj error) {
	makeResp(c, 400, obj)
}
