package proxyserver

import (
	"errors"
)

var (
	roomMgr = &RoomManager{
		Rooms: make(map[int64]*Room, 0),
	}
)

type RoomManager struct {
	Rooms map[int64]*Room
}

func GetRoomManager() *RoomManager {
	return roomMgr
}

func (mgr *RoomManager) CreateRoom(addr string) (int64, error) {

	room, err := CreateRoom(addr)
	if err != nil {
		return 0, err
	}

	mgr.AddRoom(room)

	return room.ID, nil
}

func (mgr *RoomManager) JoinRoom(roomID int64, addr string) error {
	room, err := mgr.MustGetRoom(roomID)
	if err != nil {
		return err
	}

	if room.B != nil {
		return errors.New("room already full")
	}

	room.B, err = getConn(addr)
	if err != nil {
		return err
	}

	room.AddLog("B join:%v", addr)

	return nil

}

func (mgr *RoomManager) AddRoom(room *Room) {
	mgr.Rooms[room.ID] = room
}

func (mgr *RoomManager) GetRoom(roomID int64) *Room {
	return mgr.Rooms[roomID]
}

func (mgr *RoomManager) MustGetRoom(roomID int64) (*Room, error) {
	room := mgr.GetRoom(roomID)
	if room == nil {
		return nil, errors.New("invalid room id")
	}
	return room, nil
}

func (mgr *RoomManager) DestroyRoom(roomID int64) error {
	room, err := mgr.MustGetRoom(roomID)
	if err != nil {
		return err
	}
	if room.A != nil {
		room.A.Close()
	}
	if room.B != nil {
		room.B.Close()
	}
	delete(mgr.Rooms, roomID)

	return nil
}
