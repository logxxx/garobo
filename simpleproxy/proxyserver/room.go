package proxyserver

import (
	"errors"
	"fmt"
	"net"
	"sync"
	"sync/atomic"
	"time"
)

var (
	roomIdx = int64(0)

	RoomStatusIdle    = "idle"
	RoomStatusWorking = "working"
	RoomStatusStopped = "stopped"
)

type Room struct {
	ID      int64
	Status  string
	Logs    []string
	A       net.Conn
	AStatus string
	B       net.Conn
	BStatus string
}

func CreateRoom(addr string) (*Room, error) {
	connA, err := getConn(addr)
	if err != nil {
		return nil, err
	}
	room := &Room{
		ID: atomic.AddInt64(&roomIdx, 1),
		A:  connA,
		B:  nil,
	}
	room.AddLog("room created. ID:%v A:%v", room.ID, addr)
	room.Status = RoomStatusIdle
	return room, nil
}

func (r *Room) StartWork() error {
	if r.A == nil || r.B == nil {
		return errors.New("someone not come in!")
	}

	go func() {
		wg := sync.WaitGroup{}
		wg.Add(2)

		go connCopy(r.A, r.B, &wg)
		go connCopy(r.B, r.A, &wg)

		r.Status = RoomStatusWorking
		wg.Wait()
		r.Status = RoomStatusStopped
		r.AddLog("[room%v] work stopped.", r.ID)
	}()

	r.AddLog("[room%v] start to work...", r.ID)

	return nil
}

func (r *Room) AddLog(format string, v ...interface{}) {
	input := fmt.Sprintf(format, v...)
	content := fmt.Sprintf("[%v] %v", time.Now().Format("2006/01/02 15:04:05"), input)
	r.Logs = append(r.Logs, content)
	fmt.Println(content)
}
