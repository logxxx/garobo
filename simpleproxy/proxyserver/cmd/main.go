package main

import (
	"flag"
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/simpleproxy/proxyserver"
)

var (
	addr1   = flag.String("addr1", "", "")
	addr2   = flag.String("addr2", "", "")
	apiAddr = flag.String("api_addr", "", "")
)

func main() {

	flag.Parse()

	if *addr1 == "" || *addr2 == "" || *apiAddr == "" {
		panic("empty addr")
	}

	go proxyserver.RegisterAPI(*apiAddr)

	log.Infof("start proxy addr1:%v addr2:%v", *addr1, *addr2)

	err := proxyserver.StartProxy(*addr1, *addr2)
	if err != nil {
		log.Errorf("StartProxy err:%v", err)
	}
}
