package proxyserver

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/pkg/runutil"
	"io"
	"net"
	"time"
)

func handleConn(conn net.Conn) {
	for {
		//先读header
		headData := make([]byte, 8)
		_, err := io.ReadFull(conn, headData)
		if err != nil {
			log.Infof("HandleConn2 ReadFull headData err:%v", err)
			break
		}
		//解析header
		msgLen, err := getMsgLen(headData)
		if err != nil {
			log.Infof("HandleConn2 getMsgLen err:%v", err)
			break
		}
		log.Infof("HandleConn2 get msgLen:%v", msgLen)

		if msgLen <= 0 {
			time.Sleep(1 * time.Second)
			continue
		}

		//有数据
		msg := make([]byte, msgLen)

		_, err = io.ReadFull(conn, msg)
		if err != nil {
			log.Infof("HandleConn2 ReadFull msg err:%v", err)
			break
		}

		runutil.RunSafe(func() {
			log.Infof("收到了消息:%v", string(msg))
		})
	}
}

func getMsgLen(header []byte) (uint32, error) {
	if len(header) < 8 {
		return 0, errors.New("invalid header len")
	}

	headerBuf := bytes.NewReader(header)

	//读magic number
	var magicNumber uint32
	if err := binary.Read(headerBuf, binary.BigEndian, &magicNumber); err != nil {
		return 0, errors.New("parse magicNumber failed")
	}
	if magicNumber != 0x123456 {
		return 0, fmt.Errorf("invalid magicNumber:%v except:%v", magicNumber, 0x123456)
	}

	//读消息长度
	var msgLen uint32
	if err := binary.Read(headerBuf, binary.BigEndian, &msgLen); err != nil {
		return 0, errors.New("parse msgLen failed")
	}

	return msgLen, nil

}
