package proxyserver

import (
	"fmt"
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/simpleproxy/proxyserver/config"
	"io"
	"net"
	"sync"
)

func StartProxy(addr1, addr2 string) error {

	var conn1 net.Conn
	var conn2 net.Conn

	defer conn1.Close()
	defer conn2.Close()

	var err error
	var wg sync.WaitGroup
	wg.Add(2)
	go func() {
		conn1, err = getConn(addr1)
		if err != nil {
			log.Errorf("StartProxy getConn err:%v addr1:%v", err, addr1)
		} else {
			log.Infof("StartProxy getConn1 succ:%v", addr1)
		}

		wg.Done()
	}()

	go func() {
		conn2, err = getConn(addr2)
		if err != nil {
			log.Errorf("StartProxy getConn err:%v addr2:%v", err, addr2)
		} else {
			log.Infof("StartProxy getConn2 succ:%v", addr2)
		}

		wg.Done()
	}()

	wg.Wait()

	transmit(conn1, conn2)

	log.Infof("StartProxy finish!")

	return nil
}

func transmit(conn1, conn2 net.Conn) {

	if conn1 == nil {
		log.Errorf("transmit failed: conn1 == nil")
		return
	}

	if conn2 == nil {
		log.Errorf("transmit failed: conn2 == nil")
		return
	}

	log.Infof("start transmit %v <--> %v", conn1.LocalAddr().String(), conn2.LocalAddr().String())

	wg := sync.WaitGroup{}
	wg.Add(2)

	go connCopy(conn1, conn2, &wg)
	go connCopy(conn2, conn1, &wg)

	wg.Wait()

	log.Infof("transmit finish %v <--> %v", conn1.LocalAddr().String(), conn2.LocalAddr().String())
}

func connCopy(conn1, conn2 net.Conn, wg *sync.WaitGroup) {

	_, err := io.Copy(conn1, conn2)
	if err != nil {
		log.Errorf("connCopy err:%v", err)
	}
	wg.Done()
}

func WaitForRegister() error {
	listenAddr := fmt.Sprintf(":%v", config.GetConfig().RegisterPort)
	listenAddrObj, err := net.ResolveTCPAddr("tcp", listenAddr)
	if err != nil {
		log.Errorf("WaitForRegister ResolveTCPAddr err:%v p1:%v", err, listenAddr)
		return err
	}

	listener, err := net.ListenTCP("tcp", listenAddrObj)
	if err != nil {
		log.Errorf("WaitForRegister ListenTCP err:%v p1:%v", err, listenAddr)
		return err
	}

	for {
		conn, err := listener.AcceptTCP()
		if err != nil {
			log.Errorf("WaitForRegister AcceptTCP err:%v", err)
			return err
		}
		log.Infof("一个Conn进来了:%v", conn.RemoteAddr().String())
		go handleConn(conn)
	}
}

func getConn(proxyAddr string) (net.Conn, error) {

	proxyAddrObj, err := net.ResolveTCPAddr("tcp", proxyAddr)
	if err != nil {
		log.Errorf("getConn ResolveTCPAddr err:%v p1:%v", err, proxyAddr)
		return nil, err
	}

	listener, err := net.ListenTCP("tcp", proxyAddrObj)
	if err != nil {
		log.Errorf("getConn ListenTCP err:%v p1:%v", err, proxyAddr)
		return nil, err
	}

	for {
		conn, err := listener.AcceptTCP()
		if err != nil {
			log.Errorf("getConn Accept err:%v", err)
			return nil, err
		}
		return conn, nil
	}
}
