package proxyserver

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"time"
)

func RegisterAPI(addr string) {
	g := gin.Default()
	g.GET("/ping", func(c *gin.Context) {
		c.String(200, fmt.Sprintf("pong!%v", time.Now().Format("2006-01-02 15:04:05")))
	})
	g.Run(addr)
}
