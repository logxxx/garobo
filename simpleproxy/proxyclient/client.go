package proxyclient

import (
	"bytes"
	"encoding/binary"
	"errors"
	"fmt"
	"garabo.logxxx/pkg/log"
	"io"
	"net"
)

type ProxyClient struct {
	Conn net.Conn
}

func NewClient(rAddr string, handler func([]byte)) (*ProxyClient, error) {
	conn, err := newConn(rAddr)
	if err != nil {
		return nil, err
	}

	go handleMsg(conn, handler)

	client := &ProxyClient{Conn: conn}

	return client, nil

}

func (c *ProxyClient) Send(data []byte) error {

	pkg := genePacket(data)

	_, err := c.Conn.Write(pkg)
	if err != nil {
		log.Errorf("ProxyClient.Send err:%v", err)
		return err
	}
	return nil
}

func (c *ProxyClient) Close() {
	if c.Conn != nil {
		c.Conn.Close()
	}
}

func genePacket(rawData []byte) []byte {
	//组装数据
	magicNum := make([]byte, 4)
	binary.BigEndian.PutUint32(magicNum, 0x123456)
	lenNum := make([]byte, 4)
	lenData := uint32(len(rawData))
	binary.BigEndian.PutUint32(lenNum, lenData)

	packetBuf := bytes.NewBuffer(magicNum)
	packetBuf.Write(lenNum)
	packetBuf.Write(rawData)

	return packetBuf.Bytes()
}

func handleMsg(conn net.Conn, handler func(data []byte)) {
	for {
		//先读header
		headData := make([]byte, 8)
		_, err := io.ReadFull(conn, headData)
		if err != nil {
			log.Infof("handleMsg ReadFull headData err:%v", err)
			continue
		}
		//解析header
		msgLen, err := getMsgLen(headData)
		if err != nil {
			log.Errorf("handleMsg getMsgLen err:%v", err)
			continue
		}
		//log.Infof("handleMsg get msgLen:%v", msgLen)

		if msgLen <= 0 {
			continue
		}

		//有数据
		msg := make([]byte, msgLen)

		_, err = io.ReadFull(conn, msg)
		if err != nil {
			log.Errorf("handleMsg ReadFull msg err:%v", err)
			break
		}

		if handler != nil {
			handler(msg)
		}

	}
}

func getMsgLen(header []byte) (uint32, error) {
	if len(header) < 8 {
		return 0, errors.New("invalid header len")
	}

	headerBuf := bytes.NewReader(header)

	//读magic number
	var magicNumber uint32
	if err := binary.Read(headerBuf, binary.BigEndian, &magicNumber); err != nil {
		return 0, errors.New("parse magicNumber failed")
	}
	if magicNumber != 0x123456 {
		return 0, fmt.Errorf("invalid magicNumber:%v except:%v", magicNumber, 0x123456)
	}

	//读消息长度
	var msgLen uint32
	if err := binary.Read(headerBuf, binary.BigEndian, &msgLen); err != nil {
		return 0, errors.New("parse msgLen failed")
	}

	return msgLen, nil

}

func newConn(rAddr string) (net.Conn, error) {

	log.Infof("newConn start. rAddr:%v", rAddr)

	rAddrObj, err := net.ResolveTCPAddr("tcp", rAddr)
	if err != nil {
		log.Errorf("newConn ResolveTCPAddr err:%v rAddr:%v", err, rAddr)
		return nil, err
	}
	log.Infof("remote zone:%v ip:%v port:%v",
		rAddrObj.Zone, rAddrObj.IP, rAddrObj.Port)

	conn, err := net.DialTCP("tcp", nil, rAddrObj)
	if err != nil {
		log.Errorf("newConn DialTCP err:%v", err)
		return nil, err
	}
	err = conn.SetKeepAlive(true)
	if err != nil {
		log.Errorf("newConn SetKeepAlive err:%v", err)
		return nil, err
	}

	return conn, nil
}
