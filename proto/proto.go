package proto

type MqMessage struct {
	ResponseTo string //如果要回复，回给谁
	Router string
	Data []byte
}
