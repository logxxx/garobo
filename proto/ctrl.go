package proto

type ControlReq struct {
	MouseMoveX   int    `json:"move_x"`
	MouseMoveY   int    `json:"move_y"`
	SetMousePosX int    `json:"set_mouse_pos_x"`
	SetMousePosY int    `json:"set_mouse_pos_y"`
	MouseClick   string `json:"click"`
	InputWord    string `json:"input_words"`
}

type ControlResp struct {
	Message string `json:"message"`
}

type CaptureContinuouslyReq struct {
	Stop bool `json:"stop"`
}

type MouseToggleReq struct {
	IsRight bool `json:"is_right"`
	IsUp    bool `json:"is_up"`
}

type GetScreenResolutionResp struct {
	Width  int `json:"width"`
	Height int `json:"height"`
}
