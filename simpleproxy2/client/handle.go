package client

import (
	"context"
	"garabo.logxxx/pkg/log"
	"net"
)

func HandleMsg(ctx context.Context, conn net.Conn, msg []byte) error {
	log.Infof("客户端收到了消息:%v 来自:%v", string(msg), conn.RemoteAddr().String())
	return nil
}
