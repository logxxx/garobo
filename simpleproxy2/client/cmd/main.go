package main

import (
	"context"
	"garabo.logxxx/pkg/persistent"
	"garabo.logxxx/pkg/runutil"
	"garabo.logxxx/simpleproxy2/client"
	"garabo.logxxx/simpleproxy2/proto"
	"time"
)

var (
	ctx = context.Background()
)

func main() {

	serverAddr := ":7077"

	conn, err := persistent.CreateConn(serverAddr)
	if err != nil {
		panic(err)
	}

	runutil.RunSafe(func() {
		persistent.HandleConn(ctx, conn, client.HandleMsg)
	})

	round := 0
	for {
		time.Sleep(1 * time.Second)
		round++
		req := proto.MakeRequest(proto.RouterListIdleAddrs, nil)
		err = persistent.Send(ctx, conn, req)
		if err != nil {
			panic(err)
		}
	}

}
