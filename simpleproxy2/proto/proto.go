package proto

import "garabo.logxxx/pkg/uuid"

var (
	RouterListIdleAddrs = "list_idle_addrs"
	RouterRequestConn   = "request_conn"

	MessageTypeRequest  = "req"
	MessageTypeResponse = "resp"
)

type Message struct {
	Type      string //request or response
	RequestID string
	Router    string
	Data      interface{}
}

type RequestConnReq struct {
	ConnTo string
}

func MakeRequest(router string, data interface{}) *Message {
	return &Message{
		Type:      MessageTypeRequest,
		RequestID: uuid.New(),
		Router:    router,
		Data:      data,
	}
}
