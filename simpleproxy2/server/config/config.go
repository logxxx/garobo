package config

var (
	cfg = &Config{
		RegisterPort: 7077,
	}
)

type Config struct {
	RegisterPort int `json:"register_port"`
}

func GetConfig() *Config {
	return cfg
}
