package main

import (
	"fmt"
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/pkg/persistent"
	"garabo.logxxx/simpleproxy2/server"
	"garabo.logxxx/simpleproxy2/server/config"
)

//思路:
//服务端，只需要维持两个map:
//idle_conns: key=client.Addr value=Conn.
//working_pair: key=idx value={A:A.Conn B:B.Conn}
//服务端在创建链接后，一直监听，接收客户端的两种请求:
//1.get_idle_addr_list
//返回一个[]string,包含了idle_conns.Addrs
//2.

func main() {
	addr := fmt.Sprintf(":%v", config.GetConfig().RegisterPort)
	err := persistent.NewTcpServer(addr, server.HandleConn)
	if err != nil {
		log.Errorf("NewTcpServer err:%v", err)
	}
}
