package server

import (
	"context"
	"encoding/json"
	"fmt"
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/pkg/persistent"
	"garabo.logxxx/pkg/runutil"
	"garabo.logxxx/simpleproxy2/proto"
	"net"
	"sort"
)

var (
	IdleConns = make(map[string]net.Conn, 0)
)

func HandleConn(ctx context.Context, conn net.Conn, msg []byte) error {
	if _, ok := IdleConns[conn.RemoteAddr().String()]; !ok {
		IdleConns[conn.RemoteAddr().String()] = conn
		runutil.RunSafe(func() {
			select {
			case <-ctx.Done():
				log.Errorf("HandleConn 发现结束信号 addr:%v", conn.RemoteAddr().String())
				delete(IdleConns, conn.RemoteAddr().String())
			}
			log.Errorf("将此链接从池中移除!")
		})
	}

	log.Infof("HandleConn recv msg:%v", string(msg))

	req := &proto.Message{}
	err := json.Unmarshal(msg, req)
	if err != nil {
		log.Errorf("HandleConn Unmarshal err:%v", err)
		return err
	}

	if req.RequestID == "" {
		err = persistent.Send(ctx, conn, fmt.Sprintf("empty requestID"))
		if err != nil {
			log.Errorf("HandleConn Send err:%v", err)
			return err
		}
		return nil
	}

	switch req.Router {
	case proto.RouterListIdleAddrs:
		addrs := listIdleAddrs(conn.RemoteAddr().String())
		sort.Slice(addrs, func(i, j int) bool {
			return addrs[i] > addrs[j]
		})
		err = persistent.Send(ctx, conn, addrs)
		if err != nil {
			log.Errorf("HandleConn Send err:%v", err)
			return err
		}
	default:
		err = persistent.Send(ctx, conn, fmt.Sprintf("unknown router:%v", req.Router))
		if err != nil {
			log.Errorf("HandleConn Send err:%v", err)
			return err
		}
	}

	return nil
}

func listIdleAddrs(self string) []string {
	addrs := make([]string, 0)
	for addr := range IdleConns {
		if addr == self {
			continue
		}
		addrs = append(addrs, addr)
	}
	return addrs
}
