package main

import (
	"garabo.logxxx/client/backend/core"
	"garabo.logxxx/client/backend/modules/websocket"
	"garabo.logxxx/pkg/runutil"
)

func main() {
	err := core.InitMQClient()
	if err != nil {
		panic(err)
	}

	err = core.RecvCaptureDataFromProxy()
	if err != nil {
		panic(err)
	}

	go core.RegisterApi()

	go websocket.StartWebSocket()

	runutil.WaitForExit()
}
