package websocket

import (
	"github.com/gorilla/websocket"
	"log"
	"net/http"
)

func StartWebSocket() {
	http.HandleFunc("/acc", wsPage)
	http.ListenAndServe(":8083", nil)
}

func wsPage(w http.ResponseWriter, req *http.Request) {
	//升级协议
	conn, err := (&websocket.Upgrader{
		CheckOrigin: func(r *http.Request) bool {
			log.Printf("升级协议 ua:%v referer:%v",
				r.Header["User-Agent"], r.Header["Referer"])
			return true
		},
	}).Upgrade(w, req, nil)
	if err != nil {
		http.NotFound(w, req)
		return
	}

	log.Printf("websocket 建立链接:%v", conn.RemoteAddr().String())

	client := GetClient()
	client.Addr = conn.RemoteAddr().String()
	client.Socket = conn

	go client.read()
	go client.write()

}
