package websocket

import (
	"garabo.logxxx/pkg/log"
	"github.com/gorilla/websocket"
	"time"
)

var (
	_client = NewClient()
)

type Client struct {
	Addr      string
	Socket    *websocket.Conn
	Send      chan []byte //待发送给客户端的数据
	FirstTime time.Time   //首次连接时间
	HandleMsg func([]byte)
}

func GetClient() *Client {
	return _client
}

func NewClient() *Client {
	client := &Client{
		Addr:      "",
		Socket:    nil,
		Send:      make(chan []byte, 10),
		FirstTime: time.Now(),
	}

	return client
}

func (c *Client) EmptySendQueue() {

}

func (c *Client) SendMsg(msg []byte) {
	select {
	case c.Send <- msg:
		//log.Infof("SendToClient succ! len(msg)=%v", len(msg))
	default:
		log.Infof("SendToClient failed! chan is full.")
	}
}

// 读取客户端数据
func (c *Client) read() {
	defer func() {
		if err := recover(); err != nil {
			log.Infof("client.read panic:%v", err)
		}
	}()

	//defer func() {
	//	close(c.Send)
	//}()

	for {
		_, msg, err := c.Socket.ReadMessage()
		if err != nil {
			log.Infof("client.read ReadMessage err:%v", err)
			return
		}

		c.HandleMsg(msg)

	}

}

// 向客户端写数据
func (c *Client) write() {
	defer func() {
		if err := recover(); err != nil {
			log.Infof("client.write panic:%v", err)
		}
	}()

	defer func() {
		if c.Socket != nil {
			c.Socket.Close()
		}
	}()

	for {
		select {
		case msg, ok := <-c.Send:
			if !ok {
				log.Infof("client.write c.Send failed, so continue.")
				continue
			}
			if c.Socket == nil {
				log.Infof("client.write WriteMessage failed, because c.Socket == nil")
			} else {
				c.Socket.WriteMessage(websocket.TextMessage, msg)
				//log.Infof("client.write WriteMessage succ")
			}

		}
	}

}
