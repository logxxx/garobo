package core

import (
	"errors"
	"garabo.logxxx/client/backend/modules/websocket"
	"garabo.logxxx/pkg/imageutil"
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/simpleproxy/proxyclient"
	"sync"
)

var (
	lock         sync.Mutex
	_proxyClient *proxyclient.ProxyClient
)

func initProxy() error {
	lock.Lock()
	defer lock.Unlock()
	proxyAddr := "150.158.99.30:7332"
	var err error
	_proxyClient, err = proxyclient.NewClient(proxyAddr, func(data []byte) {
		dataBase64 := imageutil.ToBase64(data)
		log.Infof("从proxy收到了连续截屏的数据 包大小=%vkb base64编码后=%vkb", len(data)/1024, len(dataBase64)/1024)
		websocket.GetClient().SendMsg(dataBase64)
	})
	if err != nil {
		log.Errorf("initProxy NewClient err:%v", err)
		return err
	}
	return nil
}

func RecvCaptureDataFromProxy() error {
	if _proxyClient == nil {
		err := initProxy()
		if err != nil {
			log.Errorf("RecvCaptureDataFromProxy initProxy err:%v", err)
			return err
		}
	}

	lock.Lock()
	defer lock.Unlock()

	if _proxyClient == nil {
		return errors.New("_proxyClient still nil")
	}

	return nil
}
