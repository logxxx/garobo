package core

import (
	"fmt"
	"garabo.logxxx/client/backend/modules/websocket"
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/pkg/reqresp"
	"garabo.logxxx/proto"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"os"
	"strconv"
)

// api模块
// 负责接受来自前端页面的http请求，并将其转化成mq请求。

func RegisterApi() {
	g := gin.New()
	g.Use(gin.Logger())
	g.Use(cors.Default())
	g.Use(reqresp.Print)

	g.GET("/", func(c *gin.Context) {
		wd, _ := os.Getwd()
		log.Infof("wd:%v", wd)
		c.File("./client/frontend/index.html")
	})

	g.StaticFS("/metadata", gin.Dir(fmt.Sprintf("./client/frontend/metadata"), true))

	g.GET("/ping", func(c *gin.Context) {
		c.String(200, "pong")
		return
	})

	g.GET("/get_screen_resolution", func(c *gin.Context) {

	})

	g.GET("/input_words", func(c *gin.Context) {
		words := c.Query("words")
		if len(words) == 0 {
			return
		}
		log.Infof("input_words:%v", words)

		req := &proto.ControlReq{
			InputWord: words,
		}

		err := GetMQClient().RequestJSON("/control/keyboard/input", req)
		if err != nil {
			reqresp.MakeServerErr(c, err)
			return
		}

		c.String(200, "ok")
		return

	})

	g.GET("/click_mouse", func(c *gin.Context) {

		click := c.Query("click")

		req := &proto.ControlReq{
			MouseClick: click,
		}

		err := GetMQClient().RequestJSON("/control/mouse/click", req)
		if err != nil {
			reqresp.MakeServerErr(c, err)
			return
		}

		c.String(200, "ok")
		return
	})

	g.GET("/toggle_mouse", func(c *gin.Context) {
		isUp := c.Query("is_up")
		isRight := c.Query("is_right")

		req := &proto.MouseToggleReq{
			IsRight: isRight == "true",
			IsUp:    isUp == "true",
		}

		log.Infof("收到toggle请求 is_up:%v is_right:%v", req.IsUp, req.IsRight)

		err := GetMQClient().RequestJSON("/control/mouse/toggle", req)
		if err != nil {
			reqresp.MakeServerErr(c, err)
			return
		}

		c.String(200, "ok")
		return

	})

	g.GET("/set_mouse_pos", func(c *gin.Context) {
		req := &proto.ControlReq{}
		req.SetMousePosX, _ = strconv.Atoi(c.Query("x"))
		req.SetMousePosY, _ = strconv.Atoi(c.Query("y"))

		log.Infof("set_mouse_pos req:%+v", req)

		err := GetMQClient().RequestJSON("/control/mouse/set_pos", req)
		if err != nil {
			reqresp.MakeServerErr(c, err)
			return
		}

		c.String(200, "ok")
		return

	})

	g.GET("/move_mouse", func(c *gin.Context) {

		req := &proto.ControlReq{}
		req.MouseMoveX, _ = strconv.Atoi(c.Query("move_x"))
		req.MouseMoveY, _ = strconv.Atoi(c.Query("move_y"))

		err := GetMQClient().RequestJSON("/control/mouse/move", req)
		if err != nil {
			reqresp.MakeServerErr(c, err)
			return
		}

		c.String(200, "ok")
		return

	})

	g.GET("/capture", func(c *gin.Context) {

		log.Infof("recv capture request.")

		data, err := GetMQClient().RequestCapture()
		if err != nil {
			reqresp.MakeServerErr(c, err)
			return
		}

		c.Writer.Write(data)
		c.Next()
	})

	g.GET("/capture_continuously", func(c *gin.Context) {

		//1.先清理
		err := GetMQClient().EmptyQueue("capture_continuously")
		if err != nil {
			reqresp.MakeServerErr(c, err)
			return
		}

		justClean := c.Query("just_clean")
		if justClean != "" {
			log.Infof("just clean!")
			return
		}

		//2.清理websocket未消费的数据
		websocket.GetClient().EmptySendQueue()

		//3.请求远端开始截屏
		req := &proto.CaptureContinuouslyReq{}

		isStop := c.Query("is_stop")
		if isStop == "true" {
			req.Stop = true
		}

		err = GetMQClient().RequestJSON("/capture_continuously", req)
		if err != nil {
			reqresp.MakeServerErr(c, err)
			return
		}
		c.String(200, "ok")
	})

	g.Run(":8082")

}
