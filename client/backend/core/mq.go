package core

import (
	"bytes"
	"encoding/json"
	"errors"
	"fmt"
	"garabo.logxxx/client/backend/modules/websocket"
	"garabo.logxxx/pkg/handlemsg"
	"garabo.logxxx/pkg/imageutil"
	"garabo.logxxx/pkg/log"
	"garabo.logxxx/pkg/net"
	"garabo.logxxx/proto"
	"io/ioutil"
	"net/http"
	"time"
)

var (
	_mqClient *MQClient

	routerCapture = "/capture"
)

type MQClient struct {
	mqHost       string
	captureQueue chan []byte
}

func InitMQClient() error {

	_mqClient = NewMQClient("150.158.99.30")

	err := _mqClient.StartMqProxy()
	if err != nil {
		return err
	}

	return nil

}

func NewMQClient(mqHost string) *MQClient {
	return &MQClient{
		mqHost:       mqHost,
		captureQueue: make(chan []byte, 1),
	}
}

func GetMQClient() *MQClient {
	return _mqClient
}

func (c *MQClient) EmptyQueue(topic string) error {
	url := fmt.Sprintf("http://%v/api/topics/%v",
		c.mqHost+":4171", topic)

	reqBuf := bytes.NewBufferString(`{"action":"empty"}`)

	req, err := http.NewRequest(http.MethodPost, url, reqBuf)
	if err != nil {
		return err
	}
	resp, err := http.DefaultClient.Do(req)
	if err != nil {
		return err
	}

	defer resp.Body.Close()
	respBytes, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}
	log.Infof("MQClient.EmptyQueue url:%v resp:%v", url, string(respBytes))

	return nil
}

func (c *MQClient) StartMqProxy() error {

	err := handlemsg.InitMessageHandler(c.mqHost)
	if err != nil {
		return err
	}

	h := handlemsg.GetMessageHandler()

	topic := fmt.Sprintf("%v_%v_response", net.GetHostName(), "client")
	err = h.StartConsume(topic, "client", c.consumeFunc)

	if err != nil {
		return err
	}

	err = h.StartConsume("capture_continuously", "client", func(data []byte) error {
		log.Infof("收到了连续截屏的数据 包大小=%v", len(data)/1024/1024)
		websocket.GetClient().SendMsg(data)
		return nil
	})
	if err != nil {
		return err
	}

	return nil

}

func (mqClient *MQClient) RequestJSON(router string, obj interface{}) error {

	data, err := json.Marshal(obj)
	if err != nil {
		return err
	}

	err = mqClient.RequestData(router, data)
	if err != nil {
		return err
	}

	return nil
}

func (mqClient *MQClient) RequestData(router string, req []byte) error {
	topic, msg := makeToTopicAndMqMessage(router, req)

	err := handlemsg.GetMessageHandler().JSON(topic, msg)
	if err != nil {
		return err
	}

	return nil
}

// 请求截图
func (mqCliet *MQClient) RequestCapture() ([]byte, error) {

	err := GetMQClient().RequestJSON("/capture", nil)
	if err != nil {
		return nil, err
	}

	log.Infof("client send capture req succ. waitting for resp...")

	select {
	case data := <-mqCliet.captureQueue:
		dataBase64 := imageutil.ToBase64(data)
		log.Infof("client recv capture succ. len(resp):%vkb after base64:%vkb", len(data)/1024, len(dataBase64)/1024)
		return dataBase64, nil
	case <-time.After(5 * time.Second):
		log.Infof("client recv capture timeout, so quit.")
		return nil, errors.New("time out")
	}

}

func (c *MQClient) consumeFunc(data []byte) error {

	msg := &proto.MqMessage{}
	err := json.Unmarshal(data, msg)
	if err != nil {
		return err
	}
	log.Infof("client recv msg respTo:%v router:%v len(data):%v", msg.ResponseTo, msg.Router, len(msg.Data))

	if msg.Router == routerCapture {
		select {
		case c.captureQueue <- msg.Data:
		default:
		}
	}

	return nil
}

func makeToTopicAndMqMessage(router string, data []byte) (string, *proto.MqMessage) {
	reqTo := fmt.Sprintf("%v_%v_request", net.GetHostName(), "server")
	respTo := fmt.Sprintf("%v_%v_response", net.GetHostName(), "client")

	mqData := &proto.MqMessage{
		ResponseTo: respTo,
		Router:     router,
		Data:       data,
	}

	return reqTo, mqData
}
