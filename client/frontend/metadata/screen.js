var showCaptureWidth = 1280
var showCaptureHeight = 720

// 默认页面打开时为小尺寸
setCaptureSize(showCaptureWidth, showCaptureHeight)

function setCaptureSize(w, h) {
    console.log("setCaptureSize() w=", w, "h=", h)
    $("#capture_img").css('width', w)
    $("#capture_img").css('height', h)
}

$("#select_screen_resolution").change(function() {
    let val = $("#select_screen_resolution").val()
    console.log("选择了:", val)
    w = showCaptureWidth
    h = showCaptureHeight
    switch(val) {
        case 'little':
            w = 640
            h = 360
            break
        case 'small':
            w = 1280
            h = 720
            break
        case 'middle':
            w = 1336
            h = 768
            break
        case 'large':
            w = 1920
            h = 1080
            break
    }

    setCaptureSize(w, h)

})