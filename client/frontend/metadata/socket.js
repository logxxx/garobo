let ws //websocket句柄
let isAutoRefreshBySocket = false

initWebSocket()

function initWebSocket() {
    console.log("websocket开始连接")

    // 连接webSocket
    ws = new WebSocket("ws://127.0.0.1:8083/acc");

    ws.onopen = function() {
        console.log("连接websocket成功！")
    }

    ws.onmessage = function(event) {
        console.log("websocket收到了消息")
        $("#capture_img").attr('src', "data:image;base64,"+event.data)
    }

    ws.onclose = function(event) {
        console.log("websocket关闭。")
    }
}

function startOrStopSocket() {

    if(isAutoRefreshBySocket) {
        //目前是开，所以要关
        $.get("http://localhost:8082/capture_continuously?is_stop=true", function(result){
            console.log("capture_continuously stop resp:". result)
            $("#btn_refresh_websocket").text('websocket刷新')
            isAutoRefreshBySocket = false
        })
        return
    }

    //目前是关，所以要开
    if(!ws) {
        alert('socket is null!')
        return
    }

    $.get("http://localhost:8082/capture_continuously", function(result){
        console.log("capture_continuously resp:". result)
        isAutoRefreshBySocket = true
        $("#btn_refresh_websocket").text('停止websocket刷新')
    })

}

$("#btn_refresh_websocket_sync").click(function(){
    $.get("http://localhost:8082/capture_continuously?just_clean=true", function(result){

    })
});