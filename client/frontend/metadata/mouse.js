
var mouseLeftToggled = false
var isMouseMonitorOn = false
var mouseMoveOffset = 50 //鼠标移动幅度
var mousePosX = 0
var mousePosY = 0

function MouseMonitor(isOpen) {

}

function ToggleLeftMouse() {
    if(mouseLeftToggled) { //已被按下，现在要松开
        ApiToggleMouse(false, true)
        $("#btn_toggle_mouse_left").text('按住鼠标左键')
        mouseLeftToggled = false
        return
    }

    //还没按下，现在要按下
    ApiToggleMouse(false, false)
    mouseLeftToggled = true
    $("#btn_toggle_mouse_left").text('松开鼠标左键')
}

function clickMouse(isRight) {
    let reqParam
    if(isRight) {
        reqParam = {
            click: 'right',
        }
    }
    console.log("click now!req:", reqParam)
    $.get("http://localhost:8082/click_mouse", reqParam, function(result){
        console.log("click_mouse resp:". result)
    })
}

function ApiToggleMouse(isRight, isUp) {
    let reqParam = "is_right="+isRight+"&is_up="+isUp
    $.get("http://localhost:8082/toggle_mouse", reqParam, function(result){
        console.log("toggle_mouse resp:". result)
    })
}

function updateMouseMoveOffset(delta) {
    mouseMoveOffset += delta
    $("#input_mouse_move_offset").val(mouseMoveOffset)
}

function setMousePos() {

    x = mousePosX
    y = mousePosY

    console.log("start set mouse pos")

    if(!x || !y){
        console.log("return for null")
        return
    }

    remoteScreenWidth = 1920
    remoteScreenHeight = 1080

    remoteScreenX = (x / showCaptureWidth) * remoteScreenWidth
    remoteScreenY = (y / showCaptureHeight) * remoteScreenHeight

    console.log("localX:", x, "localY:", y)
    console.log("showCaptureWidth:", showCaptureWidth, "showCaptureHeight:", showCaptureHeight)
    console.log("remoteX:", remoteScreenX, "remoteY:", remoteScreenY)

    req = {
        x: Math.round(remoteScreenX),
        y: Math.round(remoteScreenY),
    }

    console.log("set mouse pos now!reqParam=", req)

    $.get("http://localhost:8082/set_mouse_pos", req, function(result){
        console.log("set mouse pos resp:". result)
    });
}

function moveMouse(x, y) {
    let reqParam = {
        move_x: x,
        move_y: y,
    }
    console.log("move_mouse now!reqParam=", reqParam)
    $.get("http://localhost:8082/move_mouse", reqParam, function(result){
        console.log("move_mouse resp:". result)
    });
}

function reportMousePosController(){
    if(!reportMousePosIntervalHandler) {
        reportMousePosIntervalHandler = setInterval(function(){setMousePos()}, reportMousePosIntervalSec)
        $("#capture_img").mousemove(function(e){
            console.log("x:", e.pageX, "y:", e.pageY)
            mousePosX = e.pageX
            mousePosY = e.pageY
        })

        $("#btn_report_mouse_pos").text('停止鼠标跟踪')
        return
    }
    clearInterval(reportMousePosIntervalHandler)
    reportMousePosIntervalHandler = null

    $("#capture_img").unbind('mousemove')
    mousePosX = null
    mousePosY = null

    $("#btn_report_mouse_pos").text('开启鼠标跟踪')
}